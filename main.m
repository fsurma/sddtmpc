% Prepare system
close all;clear
rng('default')
addpath(genpath('./visualization'));
addpath(genpath('./MPCcode'));
addpath(genpath('./fuzzy'));
addpath(genpath('./simulation'));

%% Choose scenraio and controller
CONTROLLER_TYPE = 3;% Deterministic MPC ->1, Tube MPC -> 2, SDD-TMPC -> 3
NOISE_TYPE = 6; % NONE->1, RANDOM->2, BEST-CASE->3, WORST-CASE->4, UP->5, Down->6
ENVIRONMENT_TYPE = 4; % EMPTY_SPACE->1 (5 for no terminal set), CORRIDOR->3, Close to wall ->4, Polygon-obstacle ->6/7 (moving down/moving up)
RESOLUTION = 0.1; %Resolution of the visualization	= 
MAX_ITER_PS = 60; %If SDD-TMPC, then this value is the maximum number of iterations of the solver. 

%% Parameters

[environment,visualization]=get_simulation_parameters(ENVIRONMENT_TYPE,CONTROLLER_TYPE,RESOLUTION);
[environment,visualization,system] = prepare_simulation(environment,visualization);

%% Preapre controller
controller = get_controller(environment,system,CONTROLLER_TYPE,MAX_ITER_PS);


%% Start simulation
%[~,number_of_inputs]=size(B_state);
%inputs=zeros(Horizon*number_of_inputs,1);
inputs=zeros(controller.horizon*system.number_of_inputs,1);
i=2;
for t=environment.time
    %if (CONTROLLER_TYPE==3)&&(ENVIRONMENT_TYPE<6)
    if controller.nonlcon_flag
        [warm_start,simulation_tresh]=get_warm_start(environment,system,controller,inputs);
    else
        warm_start=[inputs(3:end);0;0];
        simulation_tresh=[];
    end
    [v,inputs,fval,full_fval,exitflag,message]=controller_step(environment,system,controller,warm_start,simulation_tresh);


    d = disturbance_generator(NOISE_TYPE,environment,system);
    u = controller.ancillary_control_law(v,system.nominal_state,system.state);
    [environment,system,controller]=system_update(environment,system,controller,d,u,v,i-1);
    if check_if_crash(environment,system)
        disp('crash')
        return 
    end

    update_visualization(environment,system,visualization,i)
     if system.finished_flag
         break
     end
    i=i+1;
end
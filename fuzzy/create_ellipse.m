function [major,minor] = create_ellipse(constantx,constanty,v,beta)
%{
This function creates an ellipse (uncertanity set) based on the current state

:float constantx: constant used in caluclation, no special meaning
:float constanty: constant used in caluclation, no special meaning
:float v: velocity of the system
:float beta: paraemeter that represents slipery of the ground
:float^2 return: axes of ellipse 
%}
minor = sqrt(v)*beta^2*constanty;
%major = v*constantx*(minor+1);
major = v*constantx+minor;



end
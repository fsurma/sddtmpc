function [a_vec,b_vec,c_vec,output_func_arr,fis_params,population,fval] = train_linear_fis_with_ga(input_data,output_data,...
    rules,number_of_inputs,number_of_fuzzy_sets,number_of_output_parameters,population,number_of_iterations,monotonic_flag,size_of_population)
%{
This function trains fuzzy model.

:matrix input_data: input of the fuzzy model.
:matrix output_data: ground truth values, predictions will be compared to them.
:matrix rules: table of rules
:int number_of_fuzzy_sets:  number of fuzzy sets encoded by the current population
:int number_of_inputs:  number of inputs encoded by the current population
:int number_of_output_parameters: number of how many outputs will be ysed by output functions
:matrix population: starting population. If less than required, more population will be randomly generated
:int number_of_iterations: maximum number of iterations, then the otpimization will be stopped
:bool monotonic_flag: if true, model will be punished if the function is not monotonic. WARNING: Checking monotonicity slows down the code.
:int size_of_population: number of members per population.
:return
	:vector a_vec: vector of left verticies of tringual fuzzy membership functions
	:vector b_vec: vector of peak verticies of tringual fuzzy membership functions
	:vector c_vec: vector of right verticies of tringual fuzzy membership functions
	:matrix rules: table of rules (mappings) between input membership functions and output functions
	:(vector of funtions) output_func_vec:  Each function is an output function of TS fuzzy model
	:vector fis_params: encoded parameters of fuzzy model
	:matrix population: table of the entire last population 
	:float fval: final error
%}

number_of_output_functions = max(rules(:,end));
nvars = number_of_inputs*3*number_of_fuzzy_sets+number_of_output_parameters*number_of_output_functions;

b=zeros(number_of_inputs*2*number_of_fuzzy_sets,1);
A = zeros(number_of_inputs*2*number_of_fuzzy_sets,nvars);

for i=1:(number_of_inputs*number_of_fuzzy_sets)
    vec = zeros(1,nvars);
    vec((3*i-2):(3*i))=[1,-1,0];
    A(2*i-1,:) = vec;
    vec((3*i-2):(3*i))=[0,1,-1];
    A(2*i,:) = vec;
end

lb = zeros(nvars,1);
ub= lb;
lb(1:number_of_inputs*3*number_of_fuzzy_sets)=-1;
lb((number_of_inputs*3*number_of_fuzzy_sets+1):end)=-10;
ub(1:3*number_of_fuzzy_sets)=7;
ub((3*number_of_fuzzy_sets+1):number_of_inputs*3*number_of_fuzzy_sets)=2;
ub((number_of_inputs*3*number_of_fuzzy_sets+1):end)=10;

options = optimoptions('ga','Display','none','MaxGenerations',number_of_iterations,'InitialPopulationMatrix',population,'PopulationSize',size_of_population);
cost_function_with_params = @(x) cost_function(x,input_data,output_data,rules,number_of_inputs,number_of_fuzzy_sets,number_of_output_functions,number_of_output_parameters,monotonic_flag);

[fis_params,fval,~,~,population,~] = ga(cost_function_with_params,nvars,A,b,[],[],lb,ub,[],options);

a_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
b_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
c_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
idx=1;
for i=1:number_of_inputs
    for j=1:number_of_fuzzy_sets
        a_vec(i,j)=fis_params(idx);
        idx=idx+1;
        b_vec(i,j)=fis_params(idx);
        idx=idx+1;
        c_vec(i,j)=fis_params(idx);
        idx=idx+1;
    end
end

output_func_arr = cell(number_of_output_functions,1);
for i=1:number_of_output_functions
    if number_of_output_parameters==3
        output_func_arr{i}=@(x) fis_params(idx:(idx+1))*x'+fis_params(idx+2);
    elseif number_of_output_parameters==6
        output_func_arr{i}=@(x) fis_params(idx:(idx+1))*x'+fis_params(idx+2)+fis_params(idx+3)*x(1)*x(1)+ fis_params(idx+4)*x(2)*x(2) +fis_params(idx+5)*x(1)*x(2);
    end
    idx=idx+number_of_output_parameters;
end

end
function cost = cost_function(fis_params,input_data,output_data,rules,...
    number_of_inputs,number_of_fuzzy_sets,number_of_output_functions,number_of_params_per_output,forced_monotonicity_flag)
	
%{
This function calculates the cost based on the model between real values and predictions

:matrix fis_params: encoded parameters of fuzzy model
:matrix input_data: input of the fuzzy model.
:matrix output_data: ground truth values, predictions will be compared to them.
:int number_of_fuzzy_sets:  number of fuzzy sets encoded by the current population
:int number_of_inputs:  number of inputs encoded by the current population
:int number_of_output_functions: number of how many outputs will be predicted
:bool forced_monotonicity_flag: if true, model will be punished if the function is not monotonic. WARNING: Checking monotonicity slows down the code.
:float return: cost
%}

a_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
b_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
c_vec = zeros(number_of_inputs,number_of_fuzzy_sets);
idx=1;
for i=1:number_of_inputs
    for j=1:number_of_fuzzy_sets
        a_vec(i,j)=fis_params(idx);
        idx=idx+1;
        b_vec(i,j)=fis_params(idx);
        idx=idx+1;
        c_vec(i,j)=fis_params(idx);
        idx=idx+1;
    end
end

output_func_arr = cell(number_of_output_functions,1);
for i=1:number_of_output_functions
    if number_of_params_per_output==3
        output_func_arr{i}=@(x) fis_params(idx:(idx+1))*x'+fis_params(idx+2);
    elseif number_of_params_per_output==6
        %output_func_arr{i}=@(x) fis_params(idx:(idx+1))*x'+fis_params(idx+2);
        output_func_arr{i}=@(x) fis_params(idx:(idx+1))*x'+fis_params(idx+2)+fis_params(idx+3)*x(1)*x(1)+ fis_params(idx+4)*x(2)*x(2) +fis_params(idx+5)*x(1)*x(2);
    end
    idx=idx+number_of_params_per_output;
end

error=0;
helper= size(input_data);
helper2 = size(output_data);
out=zeros(helper2);
for i=1:helper(1)
    out(i,:) = fis_eval_square(input_data(i,:),a_vec,b_vec,c_vec,rules,output_func_arr,helper2(2));
    %error = error + sum((out - output_data(i,:)).^2);
    error = error+error_function(out(i,:) - output_data(i,:));
end
cost = error/2/helper(1);
if forced_monotonicity_flag
    error=0;
    for i=1:helper(1)
        for j=[min(helper(1),i+1),min(helper(1),i+10),min(helper(1),i+11),min(helper(1),i+20),min(helper(1),i+21)]%(i+1):helper(1)
            if all(input_data(i,:)<=input_data(j,:))
                if out(i,1)>out(j,1)
                    error =error+ 10000*(out(i,1)-out(j,1));
                end
                if out(i,2)>out(j,2)
                    error =error+ 10000*(out(i,2)-out(j,2));
                end
            end
        end
    end
    
    cost = cost+error/helper(1);
end

end
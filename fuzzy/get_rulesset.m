function rules= get_rulesset(number_of_fuzzy_sets,number_of_inputs)
%{
Creates a table of rules for each possible combination of fuzzy inputs. Combination is valid if it has one fuzzy set from each input

:matrix population: each row is a member of the population
:int number_of_fuzzy_sets:  number of fuzzy sets 
:int number_of_inputs:  number of inputs 
:matrix return: table of rules
%}
rules =zeros(number_of_fuzzy_sets^2,number_of_inputs+1);
rule_helper = ones(1,number_of_inputs);

for n=1:number_of_fuzzy_sets^2
    rules(n,:)=[rule_helper,n];
    idx=1;
    while true
        rule_helper(idx)=rule_helper(idx)+1;
        if rule_helper(idx)>number_of_fuzzy_sets
            rule_helper(idx)=1;
            idx=min(idx+1,number_of_inputs);
        else
            break
        end
    end
end
end


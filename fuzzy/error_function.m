function error = error_function(u_vec)
%{
This function is used by the cost function. It raises cost if error is negative

:float u_vec: input cost
:float return: final cost
%}
for u=u_vec
    if u<0
        error=1000*u^2;
    else
        error=u^2;
    end
    %i=i+1;

end
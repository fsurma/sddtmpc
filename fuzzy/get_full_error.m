function cost = get_full_error(input,output,a_vec,b_vec,c_vec,output_func_arr,rules,forced_monotonicity_flag)
%{
This function caluclates error between prediction and ground truth output.

:vector fis_params: input vector
:vector a_vec: vector of left verticies of tringual fuzzy membership functions
:vector b_vec: vector of peak verticies of tringual fuzzy membership functions
:vector c_vec: vector of right verticies of tringual fuzzy membership functions
:matrix rules: table of rules (mappings) between input membership functions and output functions
:(vector of funtions) output_func_arr:  Each function is an output function of TS fuzzy model
:bool forced_monotonicity_flag: if true, model will be punished if the function is not monotonic. WARNING: Checking monotonicity slows down the code.
:float return: final error
%}
error=0;
helper= size(input);
helper2 = size(output);
out=zeros(helper);
for i=1:helper(1)
    out(i,:) = fis_eval_square(input(i,:),a_vec,b_vec,c_vec,rules,output_func_arr,helper2(2));
    %error = error + sum((out - output_data(i,:)).^2);
    error = error+error_function(out(i,:) - output(i,:));
end
cost = error/2/helper(1);
if forced_monotonicity_flag
    error=0;
    for i=1:helper(1)
        for j=[min(helper(1),i+1),min(helper(1),i+10),min(helper(1),i+11),min(helper(1),i+20),min(helper(1),i+21)]%(i+1):helper(1)
            if all(input(i,:)<=input(j,:))
                if out(i,1)>out(j,1)
                    error =error+ 10000*(out(i,1)-out(j,1));
                end
                if out(i,2)>out(j,2)
                    error =error+ 10000*(out(i,2)-out(j,2));
                end
            end
        end
    end
    
    cost = cost+error/helper(1);
end
end
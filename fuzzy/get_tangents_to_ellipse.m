function b_eq =get_tangents_to_ellipse(a,b,angle,A_eq)
%{
Finds points where linse (with chosen direction) and ellipse touch

:float a: lenght of major
:float b: lenght of minor
:float angle: direction of an ellipse
:matrix A_eq: directions of lines 
:matrix return: list of points
%}
t=linspace(0,2*pi,1000);
x1=a*cos(t);
x2=b*sin(t);
xe=[x1;x2];
rot = [cos(angle),-sin(angle);sin(angle),cos(angle)];
xe=rot*xe;
Points = A_eq*xe;
b_eq=max(Points')';
end
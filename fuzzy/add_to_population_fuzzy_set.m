function restored_population= add_to_population_fuzzy_set(population,...
    old_number_of_fuzzy_sets,number_of_inputs,number_of_parameters_per_output,number_of_survivors)
%{
This function adds a new empty fuzzy set to the old pupulation

:matrix population: each row is a member of the population
:int old_number_of_fuzzy_sets:  number of fuzzy sets encoded by the current population
:int number_of_inputs:  number of inputs encoded by the current population
:int number_of_parameters_per_output:  number of paremeters per output encoded by the current population. This value also encodes what type of output function is used
:int number_of_survivors: Only the best members of the poupluation (up to value of this input) will be passed further
:matrix return: old population with encoded new fuzzy set
%}
nvars = number_of_inputs*3*(old_number_of_fuzzy_sets+1)+number_of_parameters_per_output*(old_number_of_fuzzy_sets+1)^number_of_inputs;
helper =size(population);
restored_population = zeros(helper(1),nvars);
for i=1:number_of_inputs
    idx = i*3*(old_number_of_fuzzy_sets);
    idx_res=idx+(i-1)*3;
    restored_population(:,(1+idx_res-3*(old_number_of_fuzzy_sets):idx_res))=population(:,(1+idx-3*(old_number_of_fuzzy_sets):idx));
    restored_population(:,idx_res+2)=restored_population(:,idx_res+2)+0.1;
    restored_population(:,idx_res+3)=restored_population(:,idx_res+3)+0.2;
end
idx_res = idx_res+3;
for i=1:(old_number_of_fuzzy_sets)
    for j=1:(old_number_of_fuzzy_sets)
        restored_population(:,(idx_res+1):(idx_res+number_of_parameters_per_output))=population(:,(idx+1):(idx+number_of_parameters_per_output));
        idx=idx+number_of_parameters_per_output;
        idx_res=idx_res+number_of_parameters_per_output;
    end
    idx_res=idx_res+number_of_parameters_per_output;
end
restored_population=restored_population(1:number_of_survivors,:);

end


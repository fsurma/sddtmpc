function y = tringular_fis(a,b,c,x)
%{
This function evalutes a membership function

:float a: left vertex of tringual fuzzy membership functions
:float b: peak vertex of tringual fuzzy membership functions
:float c: right vertex of tringual fuzzy membership functions
:float x: input
:float return: balue of membership
%}
if x<a
    y=0;
elseif x>c
    y=0;
elseif x<b
    y=(x-a)/(b-a);
else
    y=1-(x-b)/(c-b);
end
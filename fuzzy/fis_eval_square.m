function res = fis_eval_square(inputs,a_vec,b_vec,c_vec,rules,output_func_vec,number_of_results)
%{
This function evalutes ouput based on input and fuzzy model

:vector fis_params: input vector
:vector a_vec: vector of left verticies of tringual fuzzy membership functions
:vector b_vec: vector of peak verticies of tringual fuzzy membership functions
:vector c_vec: vector of right verticies of tringual fuzzy membership functions
:matrix rules: table of rules (mappings) between input membership functions and output functions
:(vector of funtions) output_func_vec:  Each function is an output function of TS fuzzy model
:int number_of_results:  number of outputs
:vector return: output of the model
%}

helper = size(a_vec);
number_of_inputs=helper(1);
number_of_fuzzy_sets = helper(2);

fuzzy_inputs = zeros(number_of_inputs,number_of_fuzzy_sets);
for i=1:number_of_inputs
    for j=1:number_of_fuzzy_sets
        fuzzy_inputs(i,j)=tringular_fis(a_vec(i,j),b_vec(i,j),c_vec(i,j),inputs(i));
    end
end
nominator=zeros(1,number_of_results);
denominator=0;
helper = size(rules);
for i=1:helper(1)
    u = 1;
    for j=1:number_of_inputs
        u=min(u,fuzzy_inputs(j,rules(i,j)));
    end
    if u==0
        continue
    end
    output_func = output_func_vec{rules(i,j+1)};
    try
        nominator=nominator+u*(output_func(inputs));
    catch
        qweqwe=1;
    end
    

    denominator=denominator+u;
end
if denominator==0
    res = -1000000*ones(size(nominator));
else
    res=nominator/denominator;
end
function [inputs,outputs]=prepare_data(min_v,max_v,min_b,max_b,ca,cb,output_index) 
%{
This function is used to prepare data to train fuzzy model

:float min_v: min velocity of the system
:float max_v: min velocity of the system
:float min_b: min slipery of the environment
:float max_b: max slipery of the environment
:float ca/cb: constants 
:bool output_index: if true, major is returned as output, otherwise minor
:matrix^2 return: data
%}


v_arr=min_v:0.1:max_v;
b_arr=min_b:0.1:max_b;
inputs= zeros(length(v_arr)*length(b_arr),2);
i=1;
outputs= zeros(length(v_arr)*length(b_arr),1);
for v=v_arr
    for b=b_arr
        inputs(i,:)=[v,b];
        [major,minor] = create_ellipse(ca,cb,v,b);
        if output_index==1
            outputs(i,:)=major;
        else
            outputs(i,:)=minor;
        end
        i=i+1;
    end
end
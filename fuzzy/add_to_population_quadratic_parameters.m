function new_population = add_to_population_quadratic_parameters(population,...
    old_number_of_parameters_per_output,number_of_parameters_per_output,number_of_inputs,number_of_fuzzy_sets,number_of_survivors)
%{
This function changes the number of output parameters of the output function. The more parameters, the more complex output function will be called by the model

:matrix population: each row is a member of the population
:int old_number_of_parameters_per_output:  number of paremeters per output encoded by the old population. 
:int number_of_inputs:  number of inputs encoded by the current population
:int number_of_parameters_per_output:  number of paremeters per output encoded by the current population. 
:int number_of_fuzzy_sets:  number of fuzzy sets encoded the population
:int number_of_survivors: Only the best members of the poupluation (up to value of this input) will be passed further
:matrix return: old population with encoded new output parameters
%}

new_population=zeros(number_of_survivors,3*number_of_inputs*number_of_fuzzy_sets+number_of_parameters_per_output*number_of_fuzzy_sets^number_of_inputs);
idx = 3*number_of_inputs*number_of_fuzzy_sets;
new_idx = 3*number_of_inputs*number_of_fuzzy_sets;
new_population(:,1:new_idx) = population(1:number_of_survivors,1:idx);

for i=1:number_of_fuzzy_sets^number_of_inputs
    new_population(:,(new_idx+1):(new_idx+old_number_of_parameters_per_output))=...
        population(1:number_of_survivors,(idx+1):(idx+old_number_of_parameters_per_output));
    idx=idx+old_number_of_parameters_per_output;
    new_idx=new_idx+number_of_parameters_per_output;

end

end


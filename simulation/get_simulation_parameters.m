function [environment,visualization]=get_simulation_parameters(ENVIRONMENT_TYPE,CONTROLLER_TYPE,RESOLUTION)
%{
Generates the controller struct

:int ENVIRONMENT_TYPE: type fo the enivornment
:int CONTROLLER_TYPE: type fo the controller
:int RESOLUTION: resolution of the visualization

:struct return: environment and visualization struct
%}
environment.type=ENVIRONMENT_TYPE;
environment.Ts=0.1;
environment.max_simulation_iterations=400;
environment.obstacle=[];
environment.A_env=[];
environment.b_env=[];
environment.nonlinear_flag=false;
environment.terminal_set_flag=true;
environment.nominal_state_translation=zeros(4,1);
environment.starting_velocity=[0,0];

visualization.resolution=RESOLUTION; 
visualization.iterations_per_visualization_update=3;

path.exist_flag=false;


if ((ENVIRONMENT_TYPE==1)||(ENVIRONMENT_TYPE==5))
    environment.grid_size = [-5,-5,12,12];%[-11,-10,15,15];
    environment.get_beta_func = @(x) 1;%@(x)1/(norm(5-x)+1);
    path.max_destination_iter =30;
    x = linspace(0,10,path.max_destination_iter);
    y_helper = linspace(0,2*pi,path.max_destination_iter);
    y = max(3*sin(y_helper),0)+5;
    path.positions=[x;y];
    path.destination_iter = 2;
    environment.destination = path.positions(:,path.destination_iter)';
    environment.start = path.positions(:,1)';
    path.exist_flag=true;
    if ENVIRONMENT_TYPE==5
        environment.terminal_set_flag=false;
    end

elseif ENVIRONMENT_TYPE==2
    error('Old case, not supported anymore')
elseif ENVIRONMENT_TYPE==3
    environment.grid_size = [-10,-3,10,3];%[-11,-10,15,15];
    environment.get_beta_func = @(x) 1;%@(x)1/(norm(5-x)+1);
    environment.destination = [11,0];
    environment.start = [5.5,0];
    environment.A_env=[1,8,0,0;1,-8,0,0];
    environment.b_env=[10;10];
    environment.terminal_set_flag=false;
elseif ENVIRONMENT_TYPE==4
    environment.grid_size = [0,0,10,10];%[-11,-10,15,15];
    environment.get_beta_func = @(x)1/(norm(5-x(1))+1);
    environment.destination = [8.5,0.01];
    environment.start = [2,0.01];
    environment.terminal_set_flag=false;
    if CONTROLLER_TYPE==2
        environment.nominal_state_translation(2)=0.61;
    end
elseif ((ENVIRONMENT_TYPE==6)||(ENVIRONMENT_TYPE==7))
    environment.grid_size = [0,0,5,6];
    environment.get_beta_func= @(x) (x(2)>2.99)*0.9+0.1;
    environment.destination = [3.25,3.05];
    environment.start = [2.3,3];
    if ENVIRONMENT_TYPE==6
        environment.starting_velocity=[1,-1.2];
    else
        environment.starting_velocity=[1,1.2];
    end
    environment.obstacle=Polyhedron([2.35,3;2.55,2.75;2.55,3.25]);
end
environment.ca=0.20223;
environment.cb=0.075*3;
visualization.f1 = plot_background([],environment.grid_size,visualization.resolution,...
    environment.get_beta_func,environment.start,environment.destination,environment.obstacle,...
    environment.A_env,environment.b_env);
xlabel('x')
ylabel('y')
environment.A_env=[environment.A_env;eye(4);-eye(4)];
%environment.b_env=[environment.b_env;ones(8,1)];
environment.path=path;
end


function [warm_start,tresh]=get_warm_start(environment,system,controller,inputs)
%{
Solves optimization problem to find the next nominal input 
:struct environment: struct that represents environment 
:struct system: struct that represents system 
:struct controller: struct that represents controller 
:vector inputs: solution of the optimization problem in the previous iteration
:return
	:vector warm_start: starting iteration for the iterative algorithm
	:float simulation_tresh: if cost function is lower that this value, system will stop
%}
correctness_flag = true;
number_of_all_inputs = system.number_of_inputs*controller.horizon;
try
    [~,deter_v,fval_d] =controller_step(environment,system,controller.nominal_controller,[inputs(3:end);0;0]);
    disp(fval_d)
catch
   disp('nominal MPC failed')
   deter_v=zeros(number_of_all_inputs,1);
   correctness_flag=false;
end
try
    [~,tube_v,fval] =controller_step(environment,system,controller.tube_controller,[inputs(3:end);0;0]);
    disp(fval)
catch 
   disp('TMPC failed')
   tube_v=zeros(number_of_all_inputs,1);
   correctness_flag=false;
end
if correctness_flag
    tresh = 0.2*(fval-fval_d)+fval_d;
else
    tresh=fval_d+1;
end

warm_start=zeros(number_of_all_inputs,20);
predicted_state = controller.F(1:((controller.horizon-1)*system.number_of_states),:)*system.nominal_state...
    +controller.H(1:((controller.horizon-1)*system.number_of_states),1:(system.number_of_inputs*controller.horizon-system.number_of_inputs))*inputs(3:(end));
helper=[deter_v';tube_v';inputs(3:end)',(-controller.K_terminal*(predicted_state((end-3):end)-[environment.destination,0,0]'))']';
warm_start(:,1:3)=helper;
for i=4:20
    for j=1:number_of_all_inputs
        random_element = helper(j, randi([1,3]));
        warm_start(j,i)=random_element;
%error('break - to do')
    end

end


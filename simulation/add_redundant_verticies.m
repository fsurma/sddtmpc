function vers = add_redundant_verticies(original_vers,n)
%{
This function add more vertcies inside a polygon

:matrix original_vers: left verticies of tringual fuzzy membership functions
:float n: number of new vertcies between each pair from the original matrix
:float return: table with rendundant vetcies
%}
[number_of_original_vers,diemnsion] =size(original_vers);
division = linspace(0,1,n+2);
vers = zeros(number_of_original_vers+number_of_original_vers*(number_of_original_vers-1)/2*5,diemnsion);
vers(1:number_of_original_vers,:)=original_vers;
i=number_of_original_vers+1;
for j=1:number_of_original_vers
    for k=j:number_of_original_vers
        for l=2:(n+1)
            vers(i,:)=division(l)*original_vers(j,:)+(1-division(l))*original_vers(k,:);
            i=i+1;
        end
    end
end

end


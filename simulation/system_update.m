function [environment,system,controller]=system_update(environment,system,controller,d,u,v,iteration)
%{
Updates system based on the new input and distrbunce. If previous destination was reached, the evnironment and controller are also updated.

:struct environment: struct that represents environment 
:struct system: struct that represents system 
:struct controller: struct that represents controller 
:vector d: random additive distrbunce
:vector u: input chosen by the user
:vector v: nominal input chosen by the user
:int iteration: iteration order number (used to log history)

:struct^3 reutrn: updated struct of system, environment and controller.

:struct return: controller struct
%}
system.state=system.A_state*system.state+system.B_state*u+d;
if controller.use_nominal_state
    system.nominal_state=system.A_state*system.nominal_state+system.B_state*v;
else
    system.nominal_state=system.state;
end

system.input_history(:,iteration)=u;
system.nominal_input_history(:,iteration)=u;
system.state_history(:,iteration+1)=system.state;
system.nominal_state_history(:,iteration+1)=system.nominal_state;

if norm(environment.destination'-system.nominal_state(1:2))<0.3
    if environment.path.exist_flag
        if environment.path.destination_iter==environment.path.max_destination_iter
            system.finished_flag=true;
        else
            environment.path.destination_iter=environment.path.destination_iter+1;
            environment.destination = environment.path.positions(:,environment.path.destination_iter)';
            controller = get_controller(environment,system,controller.type,controller.max_iter);

        end
    else
        system.finished_flag=true;
    end

        
end
end
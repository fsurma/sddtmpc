function disturbance =disturbance_generator(NOISE_TYPE,environment,system)
%{
Generates an additive distrbunce

:int NOISE_TYPE: This value reflects what type (none, random, repelling from obstacles, etc.) should be applied
:struct environment: struct that represents environment 
:struct system: struct that represents system 
:vector return: disturbance to be aplied to the system
%}
disturbance=zeros(4,1);
vx =system.state(3);
vy=system.state(4);
beta=environment.get_beta_func(system.state(1:2));
% beta=1;
[major,minor] = create_ellipse(environment.ca*environment.Ts,environment.cb*environment.Ts,norm([vx,vy]),beta);
% minor=major;
direction = atan2(vy,vx);
if NOISE_TYPE==1
    alfa = 0;
    a = 0;
    b = 0;
elseif NOISE_TYPE==2
    alfa = rand()*2*pi;
    a = rand()*major;
    b = rand()*minor;

elseif NOISE_TYPE==3
    a = major;
    b = minor;
    
    direction_to_goal=atan2(environment.destination(2)-system.state(2),environment.destination(1)-system.state(1));
    alfa=direction_to_goal-direction;


elseif NOISE_TYPE==4
    a = major;
    b = minor;
    direction_to_goal=atan2(environment.destination(2)-system.state(2),environment.destination(1)-system.state(1));
    alfa=pi+direction_to_goal-direction;

elseif NOISE_TYPE==5
    a = major;
    b = minor;
    alfa =pi/2-direction;
elseif NOISE_TYPE==6
    a = major;
    b = minor;
    alfa =-pi/2-direction;
end
disturbance(1:2)=[a*cos(alfa);b*sin(alfa)];

rot = [cos(direction),-sin(direction);
    sin(direction),cos(direction)];
disturbance(1:2)=rot*disturbance(1:2);

end
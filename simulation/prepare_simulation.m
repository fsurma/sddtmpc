function [environment,visualization,system] = prepare_simulation(environment,visualization)
%{
This function gerates system and links it with the environment and visualization
:struct environment: struct that represents environment 
:struct visualization: struct that represents visualization 

:struct^3 return: updated environment, updated visualization and created system
%}
environment.time = 0:environment.Ts:(environment.Ts*environment.max_simulation_iterations);
system.state_history = zeros(4,environment.max_simulation_iterations+2);
system.input_history = zeros(2,environment.max_simulation_iterations+2);
system.nominal_state_history= zeros(4,environment.max_simulation_iterations+2);
system.nominal_input_history = zeros(2,environment.max_simulation_iterations+2);

%Visualisation
visualization.f2=prepare_state_evolution_plot([0,environment.Ts*environment.max_simulation_iterations]);




%State equations and x0
system.state=[environment.start';environment.starting_velocity'];
system.nominal_state = system.state+environment.nominal_state_translation;
system.state_history(:,1)=system.state;
system.nominal_state_history(:,1)=system.state;


system.A_state = [1,0,environment.Ts,0;0,1,0,environment.Ts;0,0,1,0;0,0,0,1];
system.B_state = [0,0;0,0;environment.Ts,0;0,environment.Ts];

system.number_of_states=length(system.A_state);
[~,number_of_inputs]=size(system.B_state);
system.number_of_inputs=number_of_inputs;


system.max_velocity=2;
system.max_acceleration = 5;
environment.b_env=[environment.b_env;environment.grid_size(3);environment.grid_size(4);... 
    system.max_velocity;system.max_velocity;-environment.grid_size(1);-environment.grid_size(2);system.max_velocity;system.max_velocity];
environment.A_inp = [eye(number_of_inputs);-eye(number_of_inputs)];
environment.b_inp = ones(4,1)*system.max_acceleration;

system.finished_flag=false;
end


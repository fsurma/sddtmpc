function [v,inputs,fval,full_fval,exitflag,message] = controller_step(environment,system,controller,warm_start,simulation_tresh)
%{
Solves optimization problem to find the next nominal input 
:struct environment: struct that represents environment 
:struct system: struct that represents system 
:struct controller: struct that represents controller 
:vector warm_start: starting iteration for the iterative algorithm
:float simulation_tresh: if cost function is lower that this value, system will stop
:return
	:vector v: nominal input:
	:vector input: solution of the optimization:
	:float fval: value of the cost function returned by solver
	:float full_fval: value of the cost function including constant elment
	:int exitflag: exitflag returned by the solver (you need to check documentation fo the solvers quadprog and particleswarm)
	:struct message: massage returned by the solver (you need to check documentation fo the solvers)
%}
    grad = controller.grad_func(system.nominal_state);
    b_inequ = controller.b_inequ_function(system.nominal_state);
    cost_func = @(u) u'*controller.Hes*u+grad*u;
    if controller.nonlcon_flag
        distur_function = prepare_FIS(environment.Ts,environment.get_beta_func);
        [inputs,fval,exitflag,message] = FLMPC_solver(cost_func,warm_start,system.state,system.nominal_state,...
            system.A_state,system.B_state,controller.Ak_sysyem,controller.K_local,-ones...
            (controller.horizon*2,1)*system.max_acceleration...
            ,ones(controller.horizon*2,1)*system.max_acceleration,environment.obstacle,controller.X_position_set_full,...
            controller.X_velocity_set_full,controller.U_set_full,...
            controller.terminal_set.A,controller.terminal_set.b,controller.horizon,distur_function,simulation_tresh,controller.max_iter,controller.use_center_of_the_tube_in_nucertanity_propagation_flag);
    else
        [inputs,fval,exitflag,message]=quadprog(2*controller.Hes,grad,controller.A_inequ,b_inequ);
    end
    v = inputs(1:system.number_of_inputs);
    if isempty(v)
        disp(message)
        error('Solver did not find a solution. Check above massage.')
    end
    full_fval = controller.constant_term_func(system.nominal_state)+fval;
end


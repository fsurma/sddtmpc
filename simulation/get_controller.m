function controller = get_controller(environment,system,CONTROLLER_TYPE,MAX_ITER_PS)
%{
Generates the controller struct

:struct environment: struct that represents environment 
:struct system: struct that represents system 
:int CONTROLLER_TYPE: Type of the chosen controller (MPC,TMPC or SDD-TMPC)
:int MAX_ITER_PS: Maximum number of iterations (applicable only in case of SDD-TMPC)

:struct return: controller struct
%}
if ((~isempty(environment.obstacle))&&(CONTROLLER_TYPE<3))
    error('This is nonlinear problem. It cannot be solved with linear controller')
end

controller.horizon=5;
controller.nonlcon_flag=(CONTROLLER_TYPE==3);
if (environment.type==3)
    controller.horizon=5;
end
controller.type=CONTROLLER_TYPE;

%[tube_pos,F,H,grad_func,constant_term_func,Hes,A_inequ,b_inequ_function,terminal_set,X_position_set_full,X_velocity_set_full,U_set_full,K_local,K_terminal,Ak_sysyem,helper_obstacle]...
controller=get_MPC_params(environment,system,controller,CONTROLLER_TYPE);
%= get_MPC_params(CONTROLLER_TYPE,Horizon, A_state,B_state,destination,grid_size,max_velocity,max_acceleration,ENVIRONMENT_TYPE,ca,cb,Ts,obstacle,obstacle_polytop,A_world,b_world);

controller.max_iter=MAX_ITER_PS;
if CONTROLLER_TYPE>2
    controller.nominal_controller= get_MPC_params(environment,system,controller,1);
    controller.nominal_controller.nonlcon_flag=false;
    controller.tube_controller= get_MPC_params(environment,system,controller,2);
    controller.tube_controller.nonlcon_flag=false;
	
    if ((environment.type==3)&&(CONTROLLER_TYPE==3))
        load("seed.mat")
        rng(seed)
        controller.use_center_of_the_tube_in_nucertanity_propagation_flag=false;
    else
        controller.use_center_of_the_tube_in_nucertanity_propagation_flag=true;
    end
end
end

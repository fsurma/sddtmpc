function controller=get_MPC_params(environment,system,controller,CONTROLLER_TYPE)

%{
generates all needed parameters of MPC

:struct environment: struct that represents environment 
:struct system: struct that represents system 
:struct controller: struct that represents controller 
:int CONTROLLER_TYPE: Type of the chosen controller (MPC,TMPC or SDD-TMPC)

:struct return: controller struct
%}


ful_D=repmat([environment.destination';0;0],[controller.horizon,1]);
controller.tube_pos=[];
controller.use_nominal_state=false;
input_feasible_set_A=environment.A_inp;
input_feasible_set_b=environment.b_inp;
if CONTROLLER_TYPE==1
    
    state_feasible_set_A=environment.A_env;
    state_feasible_set_b=environment.b_env;
    controller.ancillary_control_law= @(v,nominal_state,state) v;
elseif CONTROLLER_TYPE==2||CONTROLLER_TYPE==3
    controller.use_nominal_state=true;
    feasible_set=Polyhedron(environment.A_env,environment.b_env);
    controller.X_position_set_full = get_projection_of_tube(feasible_set,1:2);
    controller.X_velocity_set_full = get_projection_of_tube(feasible_set,3:4);
    controller.U_set_full = Polyhedron(environment.A_inp,environment.b_inp);
    Q_local = diag([100,100,0.1,0.1]);
    R_local = eye(2);
    [controller.K_local,~,~]=dlqr(system.A_state,system.B_state,Q_local,R_local);
        
    controller.ancillary_control_law= @(v,nominal_state,state) v-controller.K_local*(state-nominal_state);

    controller.Ak_sysyem = system.A_state- system.B_state*controller.K_local;
    W_max = get_noise_bounds(environment.ca*environment.Ts,environment.cb*environment.Ts,...
        system.max_velocity*sqrt(2),system.max_velocity,1);
    [tube,set_pos_X_nominal,set_vel_X_nominal,set_U_nominal]=get_tube_and_feasible_sets(...
        controller.Ak_sysyem,controller.K_local,W_max,controller.X_position_set_full,controller.X_velocity_set_full,...
        controller.U_set_full,'final_projections');
    if CONTROLLER_TYPE==2
        input_feasible_set_A = set_U_nominal.A;
        input_feasible_set_b = set_U_nominal.b;
    end
    tube_pos = get_projection_of_tube(tube,1:2);
    tube_pos.minVRep();
    number_of_pos_constraints = length(set_pos_X_nominal.b);
    number_of_vel_constraints = length(set_vel_X_nominal.b);
    if CONTROLLER_TYPE==2
        state_feasible_set_A = [set_pos_X_nominal.A,zeros(number_of_pos_constraints,2);
            zeros(number_of_vel_constraints,2),set_vel_X_nominal.A];
        state_feasible_set_b=[set_pos_X_nominal.b;set_vel_X_nominal.b];
    elseif CONTROLLER_TYPE==3
        state_feasible_set_A = [controller.X_position_set_full.A,zeros(number_of_pos_constraints,2);
            zeros(number_of_vel_constraints,2),controller.X_velocity_set_full.A];
        state_feasible_set_b=[controller.X_position_set_full.b;controller.X_velocity_set_full.b];
    end
end

[F,H,F_ter,H_ter]=get_prediction_matrices(system.A_state,system.B_state,controller.horizon);
controller.F=F;
controller.H=H;
controller.F_ter=F_ter;
controller.H_ter=H_ter;
[A_states,b_states] = get_prediction_constraints(state_feasible_set_A,state_feasible_set_b,controller.horizon);
A_states_final = A_states*H;
[A_inputs,b_inputs] = get_prediction_constraints(input_feasible_set_A,input_feasible_set_b,controller.horizon);

Q = diag([100,100,1,1]);
R=eye(system.number_of_inputs);

[Q_full,R_full]=get_full_weight_matrices(Q,R,controller.horizon);
Q_terminal = diag([10,10,1,1]);

if environment.type==1
    distance = 5;
elseif environment.type==2 
    distance = 2;
elseif environment.type>5 
    distance = 0.5;
else
    distance = 0.75;
end
if environment.terminal_set_flag 
    A_terminal=[eye(4);-eye(4)];
    if CONTROLLER_TYPE==1
        b_terminal = [ones(2,1)*distance;ones(2,1)*system.max_velocity;ones(2,1)*distance;ones(2,1)*system.max_velocity];
    
    elseif CONTROLLER_TYPE==2||CONTROLLER_TYPE==3
        b_terminal = [ones(2,1)*distance;ones(2,1)*max(set_vel_X_nominal.V(:));ones(2,1)*distance;ones(2,1)*max(set_vel_X_nominal.V(:))];
    end
    terminal_set_constraints = Polyhedron(A_terminal,b_terminal);
else %No terminal set
    terminal_set_constraints = Polyhedron([1,0,0,0])&Polyhedron([-1,0,0,0]); %empty polyhedron

    
end
controller.K_terminal = dlqr(system.A_state,system.B_state,Q_terminal,R);
input_feasible_set = Polyhedron(input_feasible_set_A,input_feasible_set_b);
[termianl_set_original,P]=get_termianl_set_and_terminal_cost_matrix(input_feasible_set,terminal_set_constraints,system.A_state,system.B_state,Q,R,controller.K_terminal);
% if isempty(termianl_set_original.V)
%     error('there is no way to reach destination')
% end
controller.terminal_set=termianl_set_original+[environment.destination';0;0];
if ~environment.terminal_set_flag 
    P=Q;
end
Q_full((end-system.number_of_states+1):end,(end-system.number_of_states+1):end)=P;
   


controller.A_inequ=[A_states_final;A_inputs;controller.terminal_set.A*H_ter];%terminal_set.A*H_ter
%A_inequ=[A_states_final;A_inputs];
controller.b_inequ_function = @(x0) [b_states-A_states*F*x0;b_inputs;controller.terminal_set.b-controller.terminal_set.A*F_ter*x0];%terminal_set.b-terminal_set.A*F_ter*x0
%b_inequ_function = @(x0) [b_states-A_states*F*x0;b_inputs];
controller.Hes=H'*Q_full*H+R_full;
controller.grad_func =@(x) 2*(x'*F'-ful_D')*Q_full*H;%2*nominal_state'*F'*Q_full*H;
controller.constant_term_func = @(x) x'*F'*Q_full*F*x+ful_D'*Q_full*ful_D-2*x'*F'*Q_full*ful_D;
end


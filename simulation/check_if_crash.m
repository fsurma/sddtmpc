function flag = check_if_crash(environment,system)

%{
Check if there is a crush with obstacle

:struct environment: struct that represents environment 
:struct system: struct that represents system 
:bool return: true if collision
%}

flag=false;
for obstacle=environment.obstacle
    flag = max(obstacle.A*system.state(1:2)-obstacle.b)<0;
    if flag
        break
    end
end
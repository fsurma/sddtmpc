# Implementation of SDD-TMPC (state-dependent dynamic tube model predictive control

## Project description 

The code of this repository is used to implement and test a new algorithm called SDD-TMPC. It is a control algorithm that sacrifices a bit of optimality (much less than tube MPC) but returns robust solutions (more about it you can find in [1]). In this repository, MPC, TMPC, and SDD-TMPC were implemented. "MPCcode" contains the entire code needed to run all of these algorithms. In TMPC code, the comments often refer to the equations from [2]. This part of the project does not depend on the rest of the code so you copy it and use it to control your system. It is important to notice that SDD-TMPC was called FLMPC in the early part of its development. This is how the algorithm is sometimes called in the code.

SDD-TMPC needs to have a model of boundaries of future disturbance. I used a fuzzy logic-based model trained with a genetic algorithm. The entire code needed to train the models can be found in the folder "fuzzy".

4 scenarios were created to compare all methods:
1. Following a path in an empty trajectory
2. Moving closely to the wall
3. Moving through a narrow corridor.
4. Avoiding an obstacle

More information can be found in [1]. All code needed to run the simulation (excluding the main function) can be found in the folder "simulation". Moreover, all code needed to run the visualization was included in the folder "visualization". Folder figures contain all generated Matlab figures. 

As training fuzzy model and generating a static tube for TMPC takes a lot of time and those parts of the code can be calculated offline before the beginning of the mission, they were already generated as mat files. "major.mat" and "minor.mat" is needed to run the fuzzy model. If you want to generate your own fuzzy model, you have to run "train_fis_major.m" and "train_fis_minor.m". The "final_projections.mat" contains the static tube and its projections. To recreate, you can delete this file. The main script will generate it if the final_projections.mat is not found.

The folder "figures" contain figures used in [1] as fig files so the data can be investigated. The explanation behind the figures can be found in [1].

## How to use

To use the code, you need Matlab (the code was tested for 2022a), and listed dependencies. To run the simulation, you need to run the main.m. At the beginning of this file (lines from 10 to 14), you can find 5 variables. By adjusting those variables, you can choose the type the controller, environment, resolution, type of disturbance affecting the environment, and the maximum number of iterations of the particle swarm solver. All needed information can be found in the comments. YOU DO NOT NEED TO CHANGE ANY OTHER VARIABLES.

## Dependencies

Matlab's optimization toolbox (to solve quadratic programming) - https://nl.mathworks.com/products/optimization.html

Matlab's global optimization toolbox (to use particle swarm and genetic algorithm) - https://nl.mathworks.com/products/global-optimization.html

MPT3 (for set operations) - https://www.mpt3.org/

## Known problems

The software needs a lot of time to run the simulation if SDD-TMPC is used. It happens because using polyhedron computation does not scale well with the horizon. Even though it is probably possible to speed up the current version, the amount of required time will still be very high for higher horizons. In the future, more conservative but easier-to-compute descriptions of tubes will be required (for instance ellipsoids)

To solve the optimization problem of SDD-TMPC, the particle swarm algorithm was used. It is a global optimization algorithm and the problem is, we cannot guarantee that global minimum is found. Because of it, final trajectories depend on the random generator. We use shifted trajectory from the previous iteration as a starting set of decision variables but it is not a perfect solution and multi-start would be too time-consuming.

Stage 3 with SDD-TMPC will not always return a collision-free trajectory (it depends on the seed). It happens because a simplification (see equation 16 from [1]) was made to speed up the computation. We assumed that the state of the robot does not change too rapidly which is usually true. However, in stage 3, a robot can decide (depending on found local minimum) to turn sharply what quickly changes direction. The assumption is not satisfied so predictions are not fully correct and, as a result, the robot lands in a state in which there is no safe option. For the current way of describing a tube, there is not much that can be done.  

## License

The software is distributed under the MIT license:


Copyright (c) 2023 Filip Surma

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE

## References

1. Surma, F. Jamshidnejad, A.(2023). State-Dependent Dynamic Tube MPC: A Novel tube MPC Method with a Fuzzy Model of Disturbances [Manuscript submitted for publication]. Aerospace Engineering, Tu Delft.

2. Rakovic SV, Kerrigan EC, Kouramas KI, Mayne DQ. Invariant approximations of the minimal robust positively Invariant set. IEEE Transactions on Automatic Control 2005;50(3):406–410.
function [c,ceq] = FLMPC_nonlcon(inputs,error_set,z,A_sys,B_sys,Ak_sys,K,...
    number_of_inputs,X_set_pos,X_set_vel,U_set,terminal_A,terminal_b,horizon,distur_function,obstacle,use_tube_center_flag)

%{
This function checks if hard constraints of SDD-TMPC are satisfited in the chosen horizon. 

:vector inputs: traejctory of inputs
:Polyhderon error_set: The set of all errors. The code currently supports of the set with one starting point.
:vector z: nominal state
:matrix A_sys,B_sys: system/input matrices such that x{k+1}=Ax{k}+Bu{k}
:matrix Ak_sys: system matrix after applying state feedback control law
:matrix K: state feedback's gain matrix
:uint number_of_inputs: number of inputs
:Polyhderon X_set_pos: set of all feasible positions
:Polyhderon X_set_vel: set of all feasible velocities
:Polyhderon U_set: set of all feasible inputs
:Matrix/vector terminal_A,terminal_b: terminal constraints such that terminal_A*x<=terminal_b
:uint horizon: horizon of the MPC
:function distur_function: state-dependent model of distrbunce
:Polyhderon obstacle: obstacle
 :bool use_tube_center_flag: If yes, the solver will use tube's center to
propage uncertanity (see equation 16 from the paper [1]), otherwise nominal
state will be used

:float^2 return: If c<0, then the constraints are satifited. Ceq is always 0 and is added for compatiliblity with other Matlab solvers.
%}


ceq=0;
c=horizon;
nominal_error = error_set.V';
for i=1:horizon
    v=inputs((number_of_inputs*(i-1)+1):number_of_inputs*i)';
    if use_tube_center_flag
        W = distur_function(z+nominal_error);
    else
        W = distur_function(z);
    end
    
    nominal_error=Ak_sys*nominal_error;
    z = A_sys*z+B_sys*v;
    % Check normal case
    if (i>6)&&(mod(i,2)==1)
        error_set.minVRep();
    end
    error_set = W+Ak_sys*error_set;
    set_pos = get_projection_of_tube(error_set,[1,2]);
    set_vel = get_projection_of_tube(error_set,[3,4]);
    set_inp = K*error_set;
    
    if ~((z(1:2)+set_pos)<X_set_pos)
        break
    end
    if ~isempty(obstacle)
        helper=(z(1:2)+set_pos)&obstacle;
        if ~(isempty(helper.V))
            break
        end
    end
    if ~((z(3:4)+set_vel)<X_set_vel)
        break
    end
    if ~((v+set_inp)<U_set)
        break
    end
    c=c-1;

end

termina_solution = (terminal_A*z-terminal_b)*0.01;
for element=termina_solution'
    c=c+max(element,0);
end

end


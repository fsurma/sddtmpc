function W = get_W_polyhedron(z,ca,cb,beta_func,FIS_function)
%GET_W_POLYHEDRON Summary of this function goes here
%   Detailed explanation goes here
if isempty(FIS_function)
    [major,minor] = create_ellipse(ca,cb,norm(z(3:4)),beta_func(z(1:2)));
else
    out = FIS_function([norm(z(3:4)),beta_func(z(1:2))]);
    major = out(1);
    minor=out(2);
end
if major==0
    W = Polyhedron(zeros(1,4));
    return
end

A=[1,0;
  -1,0;
   0,1;
   0,-1;
   minor/major,1;
   minor/major,-1;
   -minor/major,1;
   -minor/major,-1];
helper = minor*sqrt(2);
b=[major;major;minor;minor;helper;helper;helper;helper];
if z(3)==0
    rot=1;
else
    angle = atan2(z(4),z(3));
    rot=[cos(angle),-sin(angle);sin(angle),cos(angle)];
end
W = rot*Polyhedron(A,b);
vers=[W.V,zeros(length(W.V),2)];
W=Polyhedron(vers);




end


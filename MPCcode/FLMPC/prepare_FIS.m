function FIS_func= prepare_FIS(Ts,get_beta_func)
%{
Prepares the fuzzy model of the bounds of distrbunce

:float Ts: discretization time
:function get_beta_func: function that describes how slippery changes in the environment
:function return: fuzzy model of the bounds of distrbunce
%}
myVars={'a_vec','b_vec','c_vec','ruleList','output_func_arr'};
load('major.mat',myVars{:})
FLMPC_distur_function1 = @(z) fis_eval_square(z,a_vec,b_vec,c_vec,ruleList,output_func_arr,1);
load('minor.mat',myVars{:})
FLMPC_distur_function2 = @(z) fis_eval_square(z,a_vec,b_vec,c_vec,ruleList,output_func_arr,1);
full_FLMPC_func = @(z) [FLMPC_distur_function1(z)*Ts,FLMPC_distur_function2(z)*Ts];
FIS_func = @(z) get_W_polyhedron(z,[],[],get_beta_func,full_FLMPC_func);
end


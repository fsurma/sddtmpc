function [F,H,F_terminal,H_terminal] = get_prediction_matrices(A,B,horizon)
%{
This function caluclates system matrices to calculate all future states

:matrix A, matrix B: system matricies such that x_{k+1}=Ax_k+Bu_k
:int horizon: MPC's horizon
:matrix^4 return: system matricies such that x_{k+1 to k+horizon}=Fx_k+Hu_{k to k+horizon-1} and x_{k+horizon}=F_terminal*x_k+H_terminal*u_{k to k+horizon-1}
%}
dimenstion_of_state_vector=length(A);
size_of_B=size(B);
dimenstion_of_input_vector=size_of_B(2);
F=zeros(dimenstion_of_state_vector*horizon,dimenstion_of_state_vector);
for i=1:horizon
   F(((i-1)*dimenstion_of_state_vector+1):(i*dimenstion_of_state_vector),:)=A^i;
end
H=zeros(dimenstion_of_state_vector*horizon,horizon*dimenstion_of_input_vector);
for i=1:horizon
   for j=1:min(i,horizon)
        H(((i-1)*dimenstion_of_state_vector+1):(i*dimenstion_of_state_vector),dimenstion_of_input_vector*(j-1)+(1:dimenstion_of_input_vector))=A^(i-j)*B;
   end
end 
F_terminal=F((end-(dimenstion_of_state_vector-1)):end,:);
H_terminal=H((end-(dimenstion_of_state_vector-1)):end,:);
end
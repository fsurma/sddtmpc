function Ms= get_MS(A,W,s)
%{
Implmentation of the equation 13 (you can find more information here https://spiral.imperial.ac.uk/bitstream/10044/1/712/1/Invariant%20approximations%20of%20the.pdf)

:matrix A: system matrix
:matrix W: distrbunce bounds
:uint s: constant used in the equation (more information found in the paper)
:float return: constant used in the approximation (more information found in the paper)
%}
Ms=0;
dim =length(A);
for i=1:dim
    val=0;
    basis_vec = zeros(dim,1);
    basis_vec(i)=1;
    for j=1:s
        val = val+hw((A^j)'*basis_vec,W);
    end
    Ms=max(Ms,val);
    val=0;
    basis_vec = -basis_vec;
    for j=1:s
        val = val+hw((A^j)'*basis_vec,W);
    end

end

end
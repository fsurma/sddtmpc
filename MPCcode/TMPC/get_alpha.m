function alpha = get_alpha(A,W,s)
%{
This function calculates alpha (you can find more information here https://spiral.imperial.ac.uk/bitstream/10044/1/712/1/Invariant%20approximations%20of%20the.pdf)

:matrix A: system matrix
:matrix W: distrbunce bounds
:uint s: constant used in the equation (more information found in the paper)
:float return: constant used in the approximation (more information found in the paper)
%}
number_of_constraints=length(W.b);
alpha=0;
As = A^s;
for i=1:number_of_constraints
    f = W.A(i,:);
    alpha_i = hw(As'*f',W)/W.b(i);
    alpha=max(alpha,alpha_i);
end
end
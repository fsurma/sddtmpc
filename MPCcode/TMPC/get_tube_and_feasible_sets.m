function [tube,X_set_nominal_p,X_set_nominal_v,U_set_nominal]= get_tube_and_feasible_sets(Ak,K,W,X_set_full_pos,X_set_full_vel,input_set_full,path_of_mat)
%{
This algorithm caluclates a tube and sets used by the nominal controller. As calculating tube can take a lot of time, it is possible to load it from path

:matrix Ak: The system matrix after appling the state-feedback control law
:matrix K: The state-feedback's gain matrix
:matrix W: noise bounds
:Polyhderon X_set_full_pos: set of feasible positions
:Polyhderon X_set_full_vel: set of feasible velocities
:Polyhderon input_set_full: set of feasible inputs
:string path_of_mat: a path to the existing file

:Polyhderon^4 return: tube and sets of nominal feasible positions, nominal feasible velocities and nominal feasible inputs
%}

try
    load(path_of_mat)
catch
    tube=get_minimum_linear_robust_invariant_set(Ak,W,10^(-1)*3,1,true);
    projection_v=get_projection_of_tube(tube,[3,4]);
    projection_v.minVRep()
    projection_p=get_projection_of_tube(tube,[1,2]);
    projection_p.minVRep()
    projection_input = K*tube;
    projection_input.minVRep()
    save(path_of_mat)
end

X_set_nominal_p=X_set_full_pos-projection_p;
X_set_nominal_p.minHRep();
X_set_nominal_v=X_set_full_vel-projection_v;%
X_set_nominal_v.minHRep();
U_set_nominal= input_set_full-projection_input;

end
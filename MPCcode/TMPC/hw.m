function fval = hw(c,W)
%{
Implmentation of the equation 9 of the support function (you can find more information here https://spiral.imperial.ac.uk/bitstream/10044/1/712/1/Invariant%20approximations%20of%20the.pdf)

:vector A: direction
:matrix W: distrbunce bounds
:float return: hw
%}
options = optimoptions('linprog','Display','off');
[~,fval]=linprog(-c,W.A,W.b,[],[],[],[],options);
fval=-fval;
end
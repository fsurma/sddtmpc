function W_max= get_noise_bounds(ca,cb,max_v,max_v_in_one_dimension,max_b)
%{
This function creates a noise bounds such that w in W_max no matter what the state is

:int ca/cb: constant used in the equation
:float max_v: maxium velocity in one dimension.
:float max_v_in_one_dimension: maximum velocity
:float max_b: The highest slippery.

:Polyhderon return: distrbunce bounds
%}

[major,~]=create_ellipse(ca,cb,max_v,max_b);
[major_smaller,~]=create_ellipse(ca,cb,max_v_in_one_dimension,max_b);

A = [[1,0;
    0,1;
    -1,0;
    0,-1;
    1,1;
    -1,1;
    -1,-1;
    1,-1],zeros(8,2);
    0,0,1,0;
    0,0,0,1;
    0,0,-1,0;
    0,0,0,-1];
b=[ones(4,1)*major_smaller;ones(4,1)*major*sqrt(2);ones(4,1)*0.000001*0.25];
W_max=Polyhedron(A,b);

end


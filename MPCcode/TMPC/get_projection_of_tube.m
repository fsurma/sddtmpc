function projection = get_projection_of_tube(tube,projection_dimensions)
%{
This function creates a projection of a set.

:Polyhedron tube: A set to be porjected
:vector projection_dimensions: The set will be projected on the chosen dimensions

:Polyhderon return: Projection
%}
V=tube.V;
new_V = V(:,projection_dimensions);
projection = Polyhedron(new_V);
end


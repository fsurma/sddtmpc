function tube=get_minimum_linear_robust_invariant_set(A,W,tresh,s,compute_with_verticies_flag)
%{
This function calculates tube (robust positive invariant set) (you can find more information here https://spiral.imperial.ac.uk/bitstream/10044/1/712/1/Invariant%20approximations%20of%20the.pdf)

:matrix A: system matrix
:matrix W: distrbunce bounds
:float tresh: This is an iterative algorithm. It will be finding better aproxomation until condition is met (eq 14 from the the paper).
:uint s: constant used in the equation (more information found in the paper)
:bool compute_with_verticies_flag: If yes, the tube will be calulcated based on verticies. Otherwise, it will becaclulted based on constraints. See MPT3 documentation for more details of set computation.
:Polyhedron return: tube
%}
while true
    s=s+1;
    alpha = get_alpha(A,W,s);
    if ((alpha>1)||(alpha==0))
        continue
    end
    Ms= get_MS(A,W,s);
    if alpha<(tresh/(tresh+Ms))
        break
    end
end
if compute_with_verticies_flag==0
    Fs =W;
    for i=1:s
        Fs=Fs+A^i*W;
        Fs.minHRep();
    end
    tube = (1-alpha)^(-1)*Fs;
else
    vertices = W.V;
    vertices(:,3:4)=0;
    W =Polyhedron(vertices);
    W.minVRep();
    Fs=W;
    for i=flip(1:s)
        Fs=Fs+A^i*W;
        Fs.minVRep();
    end
    tube = (1-alpha)^(-1)*Fs;
end

end
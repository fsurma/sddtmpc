function [terminal_set,P] = get_termianl_set_and_terminal_cost_matrix(input_constarints,state_constraints,A,B,Q,R,K)
%{
This function caluclates a terminal constraint and cost for chosen system, cost matricies, constraints and state-feedback control law

:Polyhedron input_constarints: Polyhedron objects that describe allowed inputs
:Polyhedron state_constraints: Polyhedron objects that describe allowed states
:matrix A, matrix B: system matricies such that x_{k+1}=Ax_k+Bu_k
:matrix Q: state cost matrix
:matrix R: input cost matrix
:matrix K: control law is described by -K*x
:Polyhedron,matrix return: Terminal set and termianl cost matrix
%}
Ak = A-B*K;
set=state_constraints;
next_set = Ak*set;
i=1;
while true
    if next_set<set
        if K*set<input_constarints
            break
        end
    end
    set=next_set&state_constraints;
    next_set = Ak*set;
    i =i+1;
    if mod(i,5)==0
        eqweq=1;
    end
end
terminal_set=set;
P = dlyap(Ak,Q+K'*R*K);

function [Q_full,R_full]=get_full_weight_matrices(Q_matrix,R_matrix,horizon)
%{
Creates diagonal matrices with matrices on the diagonal. They are used to create to caluclate cost for the entire horizon.

:matrix Q_matrix: state cost matrix
:matrix R_matrix: input cost matrix
:int horizon: MPC's horizon
:matrix^2 return: matrices with cost matrices on the diagonal
%}
Q_full=Q_matrix;
for i=1:(horizon-1)
    Q_full=blkdiag(Q_full,Q_matrix);
end
R_full=R_matrix;
for i=1:(horizon-1)
    R_full=blkdiag(R_full,R_matrix);
end
end
%{ 
This file is used to train fuzzy model that can predict bounds of uncertainty perpendicular to the movement of the robot
%}

% Prepare script
clear;clc;close all
addpath(genpath('./fuzzy'));
rng('default')
% Prepare data

ca=0.20223;%0.25*0.97569;%ca=1;
cb=0.075*3;%cb=0.3;

max_v=sqrt(8)+0.1;
max_b=1;

[input_training,output_training] = prepare_data(0.01,max_v,0,max_b,ca,cb,2);
[input_test,output_test] = prepare_data(0.06,max_v-0.05,0.05,max_b-0.05,ca,cb,2);
population=[];

number_of_inputs=2;
number_of_fuzzy_sets=3;
number_of_parameters_per_output=3;
ruleList =get_rulesset(number_of_fuzzy_sets,number_of_inputs);

number_of_simulation=1000000;
training_error=zeros(number_of_simulation,1);
validation_error=zeros(number_of_simulation,1);
monotonic_flag=false;


f1=figure();

for i=1:150
    if mod(i,100)==0
        population=population(1:150,:);
    end
    [a_vec,b_vec,c_vec,output_func_arr,x,population,fval] = train_linear_fis_with_ga(input_training,output_training,...
        ruleList,number_of_inputs,number_of_fuzzy_sets,number_of_parameters_per_output,population,20,monotonic_flag,200);%,x);
    clf()
    training_error(i)=cost_function(x,input_training,output_training,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),training_error(max(1,i-20):i),'b');
    hold on
    validation_error(i)=cost_function(x,input_test,output_test,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),validation_error(max(1,i-20):i),'r');
    pause(0.001)
end



population= add_to_population_quadratic_parameters(population,...
    number_of_parameters_per_output,6,number_of_inputs,number_of_fuzzy_sets,150);
number_of_parameters_per_output=6;

for i=151:400
    if mod(i,40)==30
       population=population(1:150,:);
    end
    
    

    [a_vec,b_vec,c_vec,output_func_arr,x,population,fval] = train_linear_fis_with_ga(input_training,output_training,...
        ruleList,number_of_inputs,number_of_fuzzy_sets,number_of_parameters_per_output,population,20,monotonic_flag,300);%,x);
    clf()
    training_error(i)=cost_function(x,input_training,output_training,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),training_error(max(1,i-20):i),'b');
    hold on
    validation_error(i)=cost_function(x,input_test,output_test,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);plot(20*(max(1,i-20):i),validation_error(max(1,i-20):i),'r');
    plot(20*(max(1,i-20):i),validation_error(max(1,i-20):i),'r');
    pause(0.001)
end


population= add_to_population_fuzzy_set(population,...
    number_of_fuzzy_sets,number_of_inputs,number_of_parameters_per_output,300);

number_of_fuzzy_sets=number_of_fuzzy_sets+1;
ruleList =get_rulesset(number_of_fuzzy_sets,number_of_inputs);


for i=401:450
    [a_vec,b_vec,c_vec,output_func_arr,x,population,fval] = train_linear_fis_with_ga(input_training,output_training,...
        ruleList,number_of_inputs,number_of_fuzzy_sets,number_of_parameters_per_output,population,20,monotonic_flag,500);%,x);
    clf()
    training_error(i)=cost_function(x,input_training,output_training,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),training_error(max(1,i-20):i),'b');
    hold on
    validation_error(i)=cost_function(x,input_test,output_test,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),validation_error(max(1,i-20):i),'r'); 
    pause(0.001)
end


input_training=[input_training;input_test];
output_training=[output_training;output_test];
[input_test1,output_test1] = prepare_data(0.03,max_v-0.025,0.025,max_b-0.025,ca,cb,2);
[input_test2,output_test2] = prepare_data(0.08,max_v,0.075,max_b,ca,cb,2);
input_test=[input_test1;input_test2];
output_test=[output_test1;output_test2];
population= add_to_population_fuzzy_set(population,...
    number_of_fuzzy_sets,number_of_inputs,number_of_parameters_per_output,500);

number_of_fuzzy_sets=number_of_fuzzy_sets+1;
ruleList =get_rulesset(number_of_fuzzy_sets,number_of_inputs);


for i=451:600
    if mod(i,100)==70
        population=population(1:300,:);
    end
    [a_vec,b_vec,c_vec,output_func_arr,x,population,fval] = train_linear_fis_with_ga(input_training,output_training,...
        ruleList,number_of_inputs,number_of_fuzzy_sets,number_of_parameters_per_output,population,20,monotonic_flag,600);%,x);
    clf()
    training_error(i)=cost_function(x,input_training,output_training,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),training_error(max(1,i-20):i),'b');
    hold on
    validation_error(i)=cost_function(x,input_test,output_test,ruleList,number_of_inputs,number_of_fuzzy_sets,max(ruleList(:)),number_of_parameters_per_output,monotonic_flag);
    plot(20*(max(1,i-20):i),validation_error(max(1,i-20):i),'r'); 
    pause(0.001)
end
save('minor')


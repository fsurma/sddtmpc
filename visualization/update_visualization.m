function update_visualization(environment,system,visualization,iteration)
%{
Updates visualization once per chosen number of iterations

:struct environment: struct that represents environment 
:struct system: struct that represents system 
:struct visualization: struct that represents visualization 
:int iteration: current iteration
%}
if (mod(iteration-1,visualization.iterations_per_visualization_update)==0)
    update_map(visualization.f1,system.state_history,system.nominal_state_history,visualization.iterations_per_visualization_update,iteration,[])
    update_state_evolution(visualization.f2,environment.time,system.input_history,system.state_history,...
        system.nominal_input_history,system.nominal_state_history,visualization.iterations_per_visualization_update,...
        iteration,environment.obstacle,environment.grid_size);

    pause(0.01)
end
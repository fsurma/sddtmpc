function plot_distance_to_obstacle(time,state_history,obstacles,grid_size,color)
%{
This function plots distance of the system from obstacles and walls since the beginning of the simulation
Warning: plotting distance to obstacle is not supported anymore. It works with walls only

:vector time: vector of sampled time
:matrix state_history: all past states
:vector obstacles: list of obstacles Warning: not supported anymore
:vector grid_size: size of the square grid
:string color: color of the plot
%}
number_of_iterations = length(time);
dist_to_obstacles=zeros(number_of_iterations,1);
angle =linspace(0,2*pi,10000);
for i=1:number_of_iterations
    dist=Inf;
    for o=obstacles
        for a=angle
            point = [cos(a)*o.x_radius+o.x,sin(a)*o.y_radius+o.y]';
            dist = min(dist,norm(point-state_history(1:2,i)));    
        end
    end
    dist = min([dist,state_history(1,i)-grid_size(1),state_history(2,i)-grid_size(2),...
        -state_history(1,i)+grid_size(3),-state_history(2,i)+grid_size(4)]);
    dist_to_obstacles(i)=dist;
end
plot(time,dist_to_obstacles,color);
end
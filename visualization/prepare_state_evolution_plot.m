function f = prepare_state_evolution_plot(time)
%{
This function creates a figure with subplots that shows how system evoloves

:vector time: discrete vector of time samples
:figure return: created figure
%}
f=figure();
subplot(6,1,1)
ylabel('position x')
hold on

xlim([min(time),max(time)])

subplot(6,1,2)
ylabel('position y')
hold on

xlim([min(time),max(time)])

subplot(6,1,3)
ylabel('velocity x')

xlim([min(time),max(time)])
hold on

subplot(6,1,4)
ylabel('velocity y')
hold on

xlim([min(time),max(time)])

subplot(6,1,5)
ylabel('acceleration x')

xlim([min(time),max(time)])
hold on

subplot(6,1,6)
ylabel('acceleration y')

xlim([min(time),max(time)])
hold on



end
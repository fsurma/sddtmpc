function f =plot_obstacles(obstacle_polytope,f)
%{
This function adds obstacles to the existing figure

:figure f: current figure
:(vector of object polyhedron) obstacle_polytope: obstacle to be ploted
:figure return: updated figure
%}
if isempty(f)
    f=figure;
else
    figure(f);
end
hold on
for o=obstacle_polytope
    plot(o);
end

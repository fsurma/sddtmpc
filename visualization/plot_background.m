function [f] = plot_background(f,size,resolution,get_beta_func,start,destination,obstacle_polytope,A,b) 
%{
This function adds background (floor and obstacles) to the existing figure

:figure f: current figure
:vector size: size of the square grid
:float resolution: resolution of the background
:function get_beta_func: this functions shows slippery of the environment
:vector start: current location of the system
:vector destination: destination
:(vector of object polyhedron) obstacle_polytope: obstacle to be ploted
:matrix A, vector b: addational linear constraints
:figure return: updated figure
%}
f=plot_floor(size,resolution,get_beta_func,f,A,b);
f=plot_obstacles(obstacle_polytope,f);
figure(f);
hold on
plot(destination(1),destination(2),'g*')
plot(start(1),start(2),'r*')

end
function update_map(f1,state_history,nominal_state_history,iterations_per_visualization_update,current_iterations,tube)
%{
This function update position trajectory on the map

:figure f1: current map
:matrix state_history: state history
:matrix nominal_state_history: nominal state history
:int iterations_per_visualization_update: how often visualization should be updated
:int current_iterations: current interation
:Polyhedron tube: 2-D projection of the tube
:figure return: updated figure
%}
figure(f1)
for t=tube
    plot(t+nominal_state_history(1:2,current_iterations),'Color','w')
end
for i=(mod(current_iterations,iterations_per_visualization_update)+iterations_per_visualization_update):iterations_per_visualization_update:current_iterations
    plot(state_history(1,(i-iterations_per_visualization_update):i),...
        state_history(2,(i-iterations_per_visualization_update):i),'Color','r')
    plot(state_history(1,i),state_history(2,i),'r*')
end
for i=(mod(current_iterations,iterations_per_visualization_update)+iterations_per_visualization_update):iterations_per_visualization_update:current_iterations
    plot(nominal_state_history(1,(i-iterations_per_visualization_update):i),...
        nominal_state_history(2,(i-iterations_per_visualization_update):i),'Color','g')
    plot(nominal_state_history(1,i),nominal_state_history(2,i),'g*')
end
end
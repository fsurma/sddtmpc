function update_state_evolution(f,time,input_history,state_history,nominal_input_history,nominal_state_history,iterations_per_visualization_update,current_iterations,obstacles,grid_size)
%{
This function updates evolution of the state on the figure

:figure f: figure with subplots
:vector time: vector with sampled time
:matrix input_history: input history
:matrix nominal_input_history: nominal input history
:matrix state_history: state history
:matrix nominal_state_history: nominal state history
:int iterations_per_visualization_update: how often visualization should be updated
:int current_iterations: current interation
:~ obstacles: it was plotting distance to the obstacles but it does not work properly for new type of obstacles
:vector grid_size: size od grid_map
%}
figure(f)
for i=1:4
    subplot(6,1,i)
    hold on
    plot(time((current_iterations-iterations_per_visualization_update):(current_iterations)),state_history(i,(current_iterations-iterations_per_visualization_update):current_iterations),'b')
    plot(time((current_iterations-iterations_per_visualization_update):(current_iterations)),nominal_state_history(i,(current_iterations-iterations_per_visualization_update):current_iterations),'r')
end
for i=1:2
    subplot(6,1,i+4)
    hold on
    plot(time(max(1,(current_iterations-iterations_per_visualization_update-1)):(current_iterations-1)),input_history(i,max(1,(current_iterations-iterations_per_visualization_update-1)):current_iterations-1),'b')
    plot(time(max(1,(current_iterations-iterations_per_visualization_update-1)):(current_iterations-1)),nominal_input_history(i,max(1,(current_iterations-iterations_per_visualization_update-1)):current_iterations-1),'r')
end    
%subplot(7,1,7)
%hold on
%plot_distance_to_obstacle(time((current_iterations-iterations_per_visualization_update):(current_iterations)),state_history(:,(current_iterations-iterations_per_visualization_update):(current_iterations)),obstacles,grid_size,'b');
%plot_distance_to_obstacle(time((current_iterations-iterations_per_visualization_update):(current_iterations)),nominal_state_history(:,(current_iterations-iterations_per_visualization_update):(current_iterations)),obstacles,grid_size,'r');
end
function draw_ellipse_from_parameters(a,b,angle)
%{
This function plots an ellipse

:float a: lenght of major
:float b: lenght of minor
:float angle: rotation of the ellipse
%}
rot = [cos(angle),-sin(angle);sin(angle),cos(angle)];

t=linspace(0,2*pi,1000);
x1=a*cos(t);
x2=b*sin(t);
xe=[x1;x2];
xe=rot*xe;
plot(xe(1,:),xe(2,:))
function [f] = plot_floor(size_of_grid,resolution,get_beta_func,f,A,b) 
%{
This function adds background (floor) to the existing figure

:figure f: current figure
:vector size_of_grid: size of the square grid
:float resolution: resolution of the background
:function get_beta_func: this functions shows slippery of the environment
:matrix A, vector b: addational linear constraints
:figure return: updated figure
%}
if isempty(f)
    f=figure;
else
    figure(f);
end

for x = size_of_grid(1):resolution:(size_of_grid(3)-resolution)
    for y = size_of_grid(2):resolution:(size_of_grid(4)-resolution)
        if ~isempty(A)
            if max(A*[x;y;0;0]-b)>0
                continue
            end
        end
        beta = get_beta_func([x,y]);
        clr = [1-beta,1-beta,max(beta,1-beta*0.5) ];
        rectangle('Position',[x y  resolution resolution], 'FaceColor', clr, 'LineStyle','none');
    end
end
colors = zeros(11,3);
i=1;
for beta=linspace(0,1,11)
    colors(i,:) = [1-beta,1-beta,max(beta,1-beta*0.5) ];
    i=1+i;
end
colormap(colors);
hc = colorbar;
cb = linspace(0,1,11);
set(hc, 'YTick',cb, 'YTickLabel',cb)
end